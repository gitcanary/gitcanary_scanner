import json
import os
import shutil
from datetime import datetime

import requests
from slugify import slugify
from git import Repo, GitCommandError

from BaseScan import scan as altscan

import argparse

from gitblamer import gitblame
from util import onerror

from distutils.dir_util import copy_tree

parser = argparse.ArgumentParser()

parser.add_argument('--dev', help='Enable Development Scan', default=False)

args = parser.parse_args()

if(args.dev):
    from config_dev import *
else:
    from config import *

# print(url)

response = requests.get(url + "repo/", headers={'Authorization': token})

repos = json.loads(response.content.decode('utf-8'))

for repo in repos:
    print(repo['name'])
    if("gitlab" in repo["url"]):
        repourl = repo["url"].replace("https://gitlab.com/", "git@gitlab.com:") + ".git"
    elif("github" in repo["url"]):
        repourl = repo["url"].replace("https://github.com/", "git@github.com:") + ".git"
    print(repourl)
    config = repo['scanner_config']
    queue = {}

    for reportType in config['reports']:
        queue[reportType] = 0


    for report in repo['reports']:
        if report['type'] in queue.keys():
            queue[report['type']] = report['id']

    mainbranch = "master"
    if('mainbranch' in config):
        mainbranch = config['mainbranch']

    mainlocation = os.path.join(disk_location, 'main', slugify(repo['name']))
    if os.path.isdir(mainlocation):
        shutil.rmtree(mainlocation, onerror=onerror)
    try:
        pulled_repo = Repo.clone_from(repourl, mainlocation, branch=mainbranch, no_single_branch=True)

        if 'base' in queue and queue['base'] is not None:
            print("Processing Base")
            location = os.path.join(disk_location, 'base', slugify(repo['name']))
            if os.path.isdir(location):
                shutil.rmtree(location, onerror=onerror)
            copy_tree(mainlocation, location)
            jsonresult = json.dumps(altscan.scan(repourl, location, config['ignore']))
            data = {
                "repo": repo['id'],
                "date": datetime.now().isoformat(),
                "type": "base",
                "json": jsonresult,
            }
            if queue['base'] == 0:
                r = requests.post(url=url + "report/", headers={'Authorization': token}, data=data)
            else:
                r = requests.put(url=url + "report/" + str(queue['base']) + '/', headers={'Authorization': token}, data=data)
            print(r)
            shutil.rmtree(location, onerror=onerror)

        if 'gitblame' in queue and queue['gitblame'] is not None:
            print("Processing Gitblame")
            location = os.path.join(disk_location, 'gitblame', slugify(repo['name']))
            if os.path.isdir(location):
                shutil.rmtree(location, onerror=onerror)
            copy_tree(mainlocation, location)
            gitblamed = gitblame(location, config['ignore'])
            data = {
                "repo": repo['id'],
                "date": datetime.now().isoformat(),
                "type": "gitblame",
                "json": json.dumps(gitblamed),
            }
            if queue['gitblame'] == 0:
                r = requests.post(url=url + "report/", headers={'Authorization': token}, data=data)
            else:
                r = requests.put(url=url + "report/" + str(queue['gitblame']) + '/', headers={'Authorization': token}, data=data)
            print(r)
            shutil.rmtree(location, onerror=onerror)
        shutil.rmtree(mainlocation, onerror=onerror)
    except GitCommandError as e:
        print(e)